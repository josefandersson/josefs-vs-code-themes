# Josef's VS Code Themes

My collection of custom made themes for VS Code.

These will mostly be based on already existing FOSS themes but more or less tweaked to my liking.

## Themes

- Josef Solarized (light)


## Notes for developing

### Debug instance of Code with live updates

- Press `F5` to open a new window with the extensions loaded.
- Open `File > Preferences > Color Themes` and pick your color theme.
- Open a file that has a language associated. The languages' configured grammar will tokenize the text and assign 'scopes' to the tokens. To examine these scopes, invoke the `Developer: Inspect Editor Tokens and Scopes` command from the Command Palette (`Ctrl+Shift+P` or `Cmd+Shift+P` on Mac) .

### Install to "real" VS Code

- To start using your extension with Visual Studio Code copy it (this directory) into the `<user home>/.vscode/extensions` folder and restart Code.
